package com.raginigiri.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.raginigiri.entity.Login_Entity;
import com.raginigiri.repository.Change_Password_Repository;
import com.raginigiri.util.Response;

@Service
public class Change_Password_Service {
	@Autowired
	private Change_Password_Repository repository;

	@SuppressWarnings("deprecation")
	public Response changePassword(Login_Entity login_Entity) {
		Date today = new Date();
		Date myDate = new Date(today.getYear(), today.getMonth() + 1, today.getDay());
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String confirmpassword = login_Entity.getConfirmpassword();
		String encode = passwordEncoder.encode(confirmpassword);
		Response response = new Response();
		Login_Entity gettingLoginDetails = repository.gettingLoginDetails(login_Entity.getUsername());
		System.out.println(gettingLoginDetails.getPassword());
		if (gettingLoginDetails != null) {
			if (login_Entity.getUsername() != null
					&& passwordEncoder.matches(login_Entity.getPassword(), gettingLoginDetails.getPassword())) {
				login_Entity.setPassword(encode);
				login_Entity.setDate(today);
				login_Entity.setDateafteronemnth(myDate);
				repository.save(login_Entity);
				response.setStatus("00");
				response.setMessage("password has been changed Successfully");
			} else {
				response.setStatus("01");
				response.setMessage("Previous Credentials are not valid");
			}
		}
		return response;

	}
}
