package com.raginigiri.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.raginigiri.entity.Login_Entity;
import com.raginigiri.repository.Login_Repository;
import com.raginigiri.util.Response;

@Service
public class Login_Service {

	@Autowired
	private Login_Repository login_Repository;

	public Response savePassword(Login_Entity entity) {

		Date today = new Date();
		@SuppressWarnings("deprecation")
		Date myDate = new Date(today.getYear(), today.getMonth() + 1, today.getDay());
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		Response response = new Response();
		if (entity.getPassword() != null && entity.getUsername() != null) {
			String encodedPw = bCryptPasswordEncoder.encode(entity.getPassword());
			String username = entity.getUsername();
			Login_Entity login_Entity = new Login_Entity();
			login_Entity.setUsername(username);
			login_Entity.setPassword(encodedPw);
			login_Entity.setDate(today);
			login_Entity.setDateafteronemnth(myDate);
			login_Repository.save(login_Entity);
			response.setStatus("00");
			response.setMessage("Data has been saved");

		} else {

			response.setStatus("01");
			response.setMessage("Data has been not saved");
		}

		return response;

	}

	@SuppressWarnings({ "deprecation", "unused" })
	public Response checkLogin(Login_Entity login_Entity) {
		Date today = new Date();
		Date myDate = new Date(today.getYear(), today.getMonth() + 1, today.getDay());
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		Response response = new Response();
		Login_Entity gettingLoginDetails = login_Repository.gettingLoginDetails(login_Entity.getUsername());
		System.out.println(gettingLoginDetails.getPassword());
		if (gettingLoginDetails != null) {
			if (passwordEncoder.matches(login_Entity.getPassword(), gettingLoginDetails.getPassword())
					&& gettingLoginDetails.getDateafteronemnth().compareTo(today) > 0 == true) {
				response.setStatus("00");
				response.setMessage("You are verified");
			} else {
				response.setStatus("01");
				response.setMessage("Credentials are not valid or Credentials has been expired");

			}
		}
		return response;
	}
}
