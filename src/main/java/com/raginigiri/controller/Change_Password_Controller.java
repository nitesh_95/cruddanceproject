package com.raginigiri.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.raginigiri.entity.Login_Entity;
import com.raginigiri.service.Change_Password_Service;
import com.raginigiri.util.Response;

@RestController
public class Change_Password_Controller {
	@Autowired
	private Change_Password_Service service;

	@PostMapping("/danceProject/changepassword_panel/changePassword")
	public Response changePassword(@RequestBody Login_Entity loginEntity) {
		return service.changePassword(loginEntity);

	}
}
