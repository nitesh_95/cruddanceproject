package com.raginigiri.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Login_Entity")
public class Login_Entity {
	@Id
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "confirmpassword")
	String confirmpassword;
	@Column(name = "date")
	private Date date;
	@Column(name = "dateafteronemnth")
	private Date dateafteronemnth;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date2) {
		this.date = date2;
	}

	public Date getDateafteronemnth() {
		return dateafteronemnth;
	}

	public void setDateafteronemnth(Date d) {
		this.dateafteronemnth = d;
	}

}